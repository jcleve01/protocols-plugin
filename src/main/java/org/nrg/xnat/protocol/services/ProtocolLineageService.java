package org.nrg.xnat.protocol.services;/*
 * org.nrg.xnat.helpers.prearchive.PrearcDatabase
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Created 3/19/14 1:20 PM
 */

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnat.protocol.entities.ProtocolLineage;

public interface ProtocolLineageService extends BaseHibernateService<ProtocolLineage> {
}
